/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 *
 * @author Mauricio
 */
public class PayrollSimulation {

    public static void main(String[] args) {
        
        EmployeeFactory employeeFactory = EmployeeFactory.getInstance();
        Employee employee = employeeFactory.getEmployee(EmployeeFactory.EmployeeType.EMPLOYEE, "Maurico", 35, 80, 0);
        Employee manager = employeeFactory.getEmployee(EmployeeFactory.EmployeeType.MANAGER, "Athan", 35, 80, 800);
        
        System.out.println("Employee paycheque: " + employee.calculatePay());
        System.out.println("Manager paycheque: " + manager.calculatePay());
    }
}

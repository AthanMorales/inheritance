/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 *
 * @author Mauricio
 */
public class Employee {
    private String name;
    private double wage;
    private double hours;

    protected Employee(String name, double wage, double hours) {
        this.name = name;
        this.wage = wage;
        this.hours = hours;
    }
    
    double calculatePay(){
        double pay = this.hours*this.wage;
        return pay; 
    }
    
}

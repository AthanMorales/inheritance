/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 *
 * @author Mauricio
 */
public class Manager extends Employee{
    private double bonus;

    public Manager(double bonus, String name, double wage, double hours) {
        super(name, wage, hours);
        this.bonus = bonus;
    }
    
    @Override
    double calculatePay(){
        double pay = super.calculatePay()+this.bonus;
        return pay; 
    }
}

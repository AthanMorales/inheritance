/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 *
 * @author Mauricio
 */
public class EmployeeFactory {
    
    public enum EmployeeType {
        EMPLOYEE, MANAGER
    };
    
    private static EmployeeFactory empFactory;
    
    private EmployeeFactory(){}
    
    public static EmployeeFactory getInstance(){
        if(empFactory == null){
            empFactory = new EmployeeFactory();
        }
        return empFactory;
    }
    
    public Employee getEmployee(EmployeeType type, String name, double wage, double hours,double bonus) {
        switch (type) {
            case EMPLOYEE:
                return new Employee(name, wage, hours);
            case MANAGER:
                return new Manager(bonus, name, wage, hours);
        }
        return null;
    }
    
}
